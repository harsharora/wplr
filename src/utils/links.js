const API_VERSIONS = {
    v2: '/rest/v2'
}

export const BASE_URLS = {
    image: 'https://res.wooplr.com/image/upload/f_auto,q_auto,fl_lossy,w_400,c_fill,ar_3:4',
    weblink: 'https://www.wooplr.com'
}

export const HOME_LINKS = {
    products: `${API_VERSIONS.v2}/advancedlookup/productsnew/top/`
}