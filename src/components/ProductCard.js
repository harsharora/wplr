import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { BASE_URLS } from '../utils/links';
import Image from 'react-lazy-image';

// For HTML decoding of product description 
const htmlDecode = innerHTML => Object.assign(document.createElement('textarea'), {innerHTML}).value;

class ProductCard extends Component {
    constructor(props) {
        super(props);
        this._renderImages = this._renderImages.bind(this);
        this._mouseLeave = this._mouseLeave.bind(this);
        this._changeImage = this._changeImage.bind(this);
        this.state = {
            currentActiveImage: 1,
            totalImages: 1,
            intervalMethod: ''
        }
    }

    componentWillMount() {
        this.setState({totalImages: this.props.product.ecommerceProductJAXB.image_pid.length});
    }

    //For changing image
    _changeImage() {
        let currentActive = this.state.currentActiveImage;
        if (currentActive == this.state.totalImages) {
            this.setState({
                currentActiveImage: 1
            })
            return;
        }
        this.setState({
            currentActiveImage: currentActive + 1
        });
    }

     //Mouse enter event of images
    _mouseEnter() {
        console.log('mouseEntering');
        this.setState({intervalMethod: setInterval(this._changeImage, 1000)});
    }

    //For resetting it to first image
    _mouseLeave() {
        console.log('leaving this');
        clearInterval(this.state.intervalMethod);
        this.setState({
            currentActiveImage: 1
        })
    }

    //For rendering of multiple images
    _renderImages = (image, index, name) => {
        const imageLink = `${BASE_URLS.image}/${image}`;
        return (
            <Image key={image}
                source={imageLink}
                className={this.state.currentActiveImage === (index + 1) ? 'active' : ''}
                alt={name} />
        )
    }

    render() {
        const {product} = this.props,
            item = product.ecommerceProductJAXB,
            {   name, 
                description, 
                availableSizes, 
                stock, 
                color, 
                webLink,
                tags,
                category,
                subCategory,
                subCategory2,
                salesPrice,
                retailPrice,
                discount
            } = item,
            productLink = `${BASE_URLS.weblink}${webLink}`;
        return (
            <li className="product-card">
                <a href={productLink} target="_blank">
                    <div className="product-image-container" onMouseLeave={this._mouseLeave} onMouseEnter={this._mouseEnter.bind(this)}>
                        {
                            item.image_pid.map((image, index) => this._renderImages(image, index, name))
                        }
                    </div>
                    <div className="pcd">
                        <div className="pcd-header">
                            <p className="pcd-title">{name}</p>
                            <p className="pcd-desc">{htmlDecode(description)}</p>
                        </div>
                        <div className="pcd-price">
                            {
                                discount &&
                                <div>
                                    <p className="pcd-price-retail">₹ {retailPrice}</p>
                                    <p className="pcd-discount">{Math.floor(((retailPrice-salesPrice)/retailPrice)*100)}% off</p>
                                    <p className="pcd-price-sales">₹ {salesPrice}</p>
                                </div>
                            }
                            {
                                !discount &&
                                <div>
                                    <p className="pcd-price-sales">₹ {retailPrice}</p>
                                </div>
                            }
                        </div>
                    </div>
                    <div className="pc-meta">
                        <div className="pc-meta-group">
                            <p className="pc-meta-helper">SIZES</p>
                            <p className="pc-meta-secondary">{availableSizes}</p>
                        </div>
                        <div className="pc-meta-group">
                            <p className="pc-meta-helper">COLORS</p>
                            <p className="pc-meta-secondary">{color}</p>
                        </div>
                    </div>
                </a>
            </li>
        )
    }
}

ProductCard.propType = {
    product: PropTypes.object.isRequired
}

export default ProductCard;