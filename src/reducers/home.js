import {
    FEED_FETCH_REQUEST,
    FEED_FETCH_FAILURE,
    FEED_FETCH_SUCCESS,
} from '../actions/constants';

const INITIAL_STATE = {
    fetching: false,
    products: [],
    error: false,
    errorMsg: null,
    page: 1,
    count: 20,
    fromCache: true,
    search: {}
}

const initializeState = () => {
    return INITIAL_STATE;
}

export default function home(state = initializeState(), action) {
    switch (action.type) {
        case FEED_FETCH_REQUEST:
            return {
                ...state,
                fetching: true
            };
        case FEED_FETCH_SUCCESS: 
            return {
                ...state,
                fetching: false,
                products: [...state.products, ...action.payload.filter(product => product.id !== -2)],
                page: ++state.page,
                search: (action.payload.filter(search => search.id === -2))[0]
            }
        case FEED_FETCH_FAILURE:
            return {
                ...state,
                fetching: false,
                error: true,
                errorMsg: action.error
            }
        default:
            return state;
    }
}