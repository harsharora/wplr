import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchFeed } from '../actions/home';
import ProductCard from '../components/ProductCard';
import '../assets/styles/home.css';

class Home extends Component {
    constructor(props) {
        super(props);
        this._renderProducts = this._renderProducts.bind(this);
        this.state = {
            rows: 3
        }
    }

    componentWillMount() {
        //TODO: refactor this
        let options = {
            sort_by: ["relevance"], 
            newFilters: []
        },
            queryParams = {
                page: this.props.home.page,
                fromCache: this.props.home.fromCache,
                count: this.props.home.count
            };
        this.props.fetchFeed(options, queryParams)
        .then(val => {
            queryParams['page'] = this.props.home.page;
            this.props.fetchFeed(options, queryParams)
        })
    }

    _updateRow = (rows) => {
        this.setState({
            rows
        })
    }

    _renderProducts(product, index){
        return ( 
            <ProductCard key={index} product={product} /> 
        )
    }

    render() {
        const { home } = this.props;
        return (
            <div className="container">
                {
                    home.products && home.products.length > 0 &&
                    <ul className="list">
                        { home.products.map((product, index) => this._renderProducts(product, index)) }
                    </ul>
                }
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        home: state.home
    }
}

export default connect(mapStateToProps, {
    fetchFeed
})(Home);