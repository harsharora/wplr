export const FEED_FETCH_REQUEST = 'FEED_FETCH_REQUEST';
export const FEED_FETCH_SUCCESS = 'FEED_FETCH_SUCCESS';
export const FEED_FETCH_FAILURE = 'FEED_FETCH_FAILURE';

export const FILTER_REQUEST = 'FILTER_REQUEST';
export const FILTER_SUCCESS = 'FILTER_SUCCESS';
export const FILTER_FAILURE = 'FILTER_FAILURE';

export const onRequest = (type) => {
    return {
        type
    }
}

export const  onFailure = (type, error) => {
    return {
        type,
        error
    }
}

export const onSuccess = (type, data) => {
    return {
        type,
        payload: data
    }
}
