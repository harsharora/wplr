import {
    FEED_FETCH_REQUEST,
    FEED_FETCH_FAILURE,
    FEED_FETCH_SUCCESS,
    onSuccess,
    onRequest,
    onFailure
} from './constants';
import axios  from 'axios';
import { BASE_URLS, HOME_LINKS } from '../utils/links';

export function fetchFeed(options = {sort_by: ["relevance"], newFilters: []}, queryParams) {
    return dispatch => {
        dispatch(onRequest(FEED_FETCH_REQUEST));
        let q = '';
        if (queryParams) {
            q += '?';
            Object.keys(queryParams).forEach(function (key) {
                q += `${key}=${queryParams[key]}&`
            });
        }
        let URL = `${BASE_URLS.weblink}${HOME_LINKS.products}${q}`;
        return axios.post(URL, options)
            .then(data => {
                dispatch(onSuccess(FEED_FETCH_SUCCESS, data.data));
                return data;
            })
            .catch(error => {
                dispatch(onFailure(FEED_FETCH_FAILURE, error));
                return error;
            })
    }
}
