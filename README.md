This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Commands to run this project

```
cd /path/to/the/directory
npm install
npm start
```
